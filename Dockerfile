# Install python 
FROM python:3.11-alpine

EXPOSE 5000
WORKDIR /src

# Install requirements first
COPY requirements.txt .
RUN pip install -r requirements.txt

# Install the project
COPY . .

# Run the project
CMD ["python", "-m", "iot"]