# IoT Project
## Get data from MQTT setup, display it, and create an API to distribute said data

## Hardware
* ESP32
* DHT22
* SSD1306

## Software
* MQTT PROTOCOL 
* Broker Mosquito
* SQLite Database

## References
* https://mqtt.org
* https://mosquito.org
* https://pypi.org/project/paho-mqtt

## Virtual environment 

### Create
`python -m venv venv`

### Delete
    Delete folder `venv`

## Requirements
    requirements.txt

## Update requirements
    pip freeze > requirements.txt

## Install Python Library requirements
    pip install -r requirements.txt

## Database
    SQLite
### Data Viz
    https://inloop.github.io/sqlite-viewer/

## Local operations

__build__
    docker build -t agen97/iot .

__run__
    docker run -d --name my-iot -p 80:5000 --link redis -v data:/src/db agen97/iot

__dependencies__
    Redis
    docker run -d --name redis -p 6379:6379 -v data:/data redis:alpine