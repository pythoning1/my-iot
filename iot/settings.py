__version__ = '1.1.1'

APP_NAME = 'IOT'

MIN_PYTHON_VERSION = (3, 8)

DEFAULT_LOG_LEVEL = 'INFO'

DATABASE = 'sqlite:///db/sensors-data.sqlite'

REDIS_URL = 'redis'