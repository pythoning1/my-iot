import sys
import logging

try:
    from iot import settings as sets
except ModuleNotFoundError:
    import settings as sets


if sys.version_info[:2] < sets.MIN_PYTHON_VERSION:
    print('Error: minimum python version required: ',
          sets.MIN_PYTHON_VERSION[0], '.', sets.MIN_PYTHON_VERSION[1],
          sep='')
    sys.exit(1)

# Create logger
logging.basicConfig(filename='my_sum.log', level=logging.INFO)

logger = logging.getLogger(__name__)
logger.info(f'Starting {sets.APP_NAME}  V{sets.__version__}')
logger.setLevel(sets.DEFAULT_LOG_LEVEL)
