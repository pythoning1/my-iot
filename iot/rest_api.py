import logging
import datetime

from flask import Flask, render_template, jsonify, request
from waitress import serve
import dataset
import redis

try:
    from iot import settings
except ModuleNotFoundError:
    import settings

app = Flask(__name__)
logger = logging.getLogger('__name__')
cache = redis.Redis(host=settings.REDIS_URL, port=6379) 


@app.route('/')
def hello_world():
    return render_template('index.html', version=settings.__version__)


@app.route('/realtime')
def realtime():
    logger.info('Realtime function call')
    data = eval(cache.get('temperature'))
    logger.debug(data)
    return jsonify(data)


@app.route('/history')
def history():
    try:
        lim = int(request.args.get('limit'))
    except (TypeError, ValueError):
        lim = 10
    logger.info('History function call')
    db = dataset.connect(settings.DATABASE)
    result = db.query('SELECT ts, value from temperature '
                        f'ORDER BY ts DESC LIMIT {lim}')
    data = [row for row in result]
    db.close()
    logger.debug(data)
    return jsonify(data)


def main():
    logger.info(f'Starting {settings.APP_NAME} (v{settings.__version__})')
    serve(app, host='0.0.0.0', port=5000, threads=5)


if __name__ == '__main__':
    logging.basicConfig(level=settings.DEFAULT_LOG_LEVEL)
    main()