# MQTT Protocol Analysis
# Author: André Neves
# Date: October 2023

import datetime as dt
import dataset
import paho.mqtt.client as mqtt
import redis

try:
    from iot import settings
except ModuleNotFoundError:
    import settings

#redis connection
cache = redis.Redis(host=settings.REDIS_URL, port=6379)

def on_connect_callback(client, user_data, flags, rc):
    print(rc)
    if rc == 0:
        print('Connection OK')
        topic = 'esp32/+/sensors/#'
        client.subscribe(topic)
        client.message_callback_add(topic, on_message_callback)
    else:
        print('Connection failed')


def on_message_callback(client, user_data, message):
    time = dt.datetime.utcnow()
    data_type = message.topic.split('/')[-1]
    data_value = float(message.payload)

    #Print message
    display_message = f"{time}: {data_type:^12} - {data_value:.1f}"
    print(display_message)

    #Update redis
    data = dict(ts=time, value=data_value)
    cache.set(data_type, str(data))

    db = dataset.connect(settings.DATABASE)
    table = db[data_type]
    table.insert(dict(ts=time, value=data_value))
    db.close()


def main():
    Client = mqtt.Client(client_id = 'andre')
    Client.on_connect = on_connect_callback
    Client.username_pw_set('cfreire', '65Zc2E')
    Client.connect('iot.cfreire.pt')
    Client.loop_forever()


if __name__ == '__main__':
    main()